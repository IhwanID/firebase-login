import 'dart:math';

import 'package:flutter/material.dart';

class RefreshScreen extends StatefulWidget {
  @override
  _RefreshScreenState createState() => _RefreshScreenState();
}

class _RefreshScreenState extends State<RefreshScreen> {
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();

  String url;

  @override
  void initState() {
    super.initState();
    url = 'https://source.unsplash.com/random';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Refresh'),
        ),
        body: RefreshIndicator(
            key: _refreshIndicatorKey,
            onRefresh: refresh,
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: ListView(
                children: <Widget>[
                  Image.network('$url'),
                  Center(child: Text('Source : $url'))
                ],
              ),
            )));
  }

  Future<void> refresh() async {
    int randomNumber = Random().nextInt(20);
    setState(() {
      url = 'https://source.unsplash.com/collection/$randomNumber';
    });
    return;
  }
}
